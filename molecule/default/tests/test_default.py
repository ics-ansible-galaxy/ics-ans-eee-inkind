import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_ess_services(host):
    install_root = '/export/nfsroots/centos7/rootfs'
    for script in ('ess_boot.py', 'procServ_vacuum.py', 'procServ'):
        assert host.file(install_root + '/usr/bin/' + script).exists
    for unit in ('ess-boot.service', 'ioc@.service', 'procServ-vacuum.service'):
        assert host.file(install_root + '/etc/systemd/system/' + unit).exists
